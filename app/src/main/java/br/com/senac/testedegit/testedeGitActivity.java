package br.com.senac.testedegit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import br.com.senac.testedegit.ClickDigito;
import br.com.senac.testedegit.R;

class testedeGGitActiv extends AppCompatActivity {

    private TextView visor;
    private Button divisao;
    private Button multiplicacao;
    private Button subtracao;
    private Button adicao;
    private Button btnCe;
    private Button btnPonto;
    private Button igual;
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;



   /* private void inicializar() {

        visor = (TextView) findViewById (R.id.visor);

        divisao = (Button)findViewById(R.id.divisao);
        multiplicacao = (Button) findViewById(R.id.multiplicacao);
        subtracao = (Button) findViewById(R.id.subtracao);
        adicao = (Button) findViewById(R.id.adicao);
        btnCe = (Button) findViewById(R.id.btnCe);
        btnPonto = (Button) findViewById(R.id.btnPonto);
        igual = (Button) findViewById(R.id.igual);
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn2);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9); } */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testede_git);

        this.btn0 = (Button) findViewById(R.id.btn0);
        this.btn1 = (Button) findViewById(R.id.btn1);
        this.btn2 = (Button) findViewById(R.id.btn2);
        this.btn3 = (Button) findViewById(R.id.btn3);
        this.btn4 = (Button) findViewById(R.id.btn4);
        this.btn5 = (Button) findViewById(R.id.btn5);
        this.btn6 = (Button) findViewById(R.id.btn6);
        this.btn7 = (Button) findViewById(R.id.btn7);
        this.btn8 = (Button) findViewById(R.id.btn8);
        this.btn9 = (Button) findViewById(R.id.btn9);
        this.btnPonto = (Button) findViewById(R.id.btnPonto);
        this.btnCe = (Button) findViewById(R.id.btnCe);
        this.adicao = (Button) findViewById(R.id.adicao);
        this.subtracao = (Button) findViewById(R.id.subtracao);
        this.multiplicacao = (Button) findViewById(R.id.multiplicacao);
        this.divisao = (Button) findViewById(R.id.divisao);
        this.igual = (Button) findViewById(R.id.igual);

        this.visor = (TextView) findViewById(R.id.visor);



        ClickDigito clickDigito = new ClickDigito(this.visor);

        this.btn0.setOnClickListener(clickDigito);
        this.btn1.setOnClickListener(clickDigito);
        this.btn2.setOnClickListener(clickDigito);
        this.btn3.setOnClickListener(clickDigito);
        this.btn4.setOnClickListener(clickDigito);
        this.btn5.setOnClickListener(clickDigito);
        this.btn6.setOnClickListener(clickDigito);
        this.btn7.setOnClickListener(clickDigito);
        this.btn8.setOnClickListener(clickDigito);
        this.btn9.setOnClickListener(clickDigito);
        this.btnPonto.setOnClickListener(clickDigito);

        this.subtracao.setOnClickListener(clickDigito);
        this.adicao.setOnClickListener(clickDigito);
        this.multiplicacao.setOnClickListener(clickDigito);
        this.divisao.setOnClickListener(clickDigito);
        this.igual.setOnClickListener(clickDigito);





    }



}