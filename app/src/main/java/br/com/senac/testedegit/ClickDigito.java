package br.com.senac.testedegit;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by sala304b on 15/08/2017.
 */
public class ClickDigito implements View.OnClickListener {

    private TextView visor;

    public ClickDigito(TextView textView) {

        this.visor = textView;
    }

    @Override
    public void onClick(View v) {

        String texto = visor.getText().toString();
        Button botao = (Button) v;
        String digito = botao.getText().toString();


        if (texto.equals("0")) {
            visor.setText(digito);
        } else {
            if (botao.getId() != R.id.btnPonto) {
                visor.setText(texto + digito);
            } else {
                if (!texto.contains(".")) {
                    visor.setText(texto + digito);
                }
            }
        }
    }




}



